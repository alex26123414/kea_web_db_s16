<?php
/**
 * Created by PhpStorm.
 * User: coag
 * Date: 01-03-2016
 * Time: 12:54
 */

$day = date("l"); //This is the letter l from Latvia.
$color = "red";

if($day == 'Tuesday'){
    $color = 'blue';
}
if($day == 'Friday'){
    $color = 'green';
}

?>

<h1 style="color: <?php echo $color;?>">
    <?php echo $day;?>
</h1>
