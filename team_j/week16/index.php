<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css"
      integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">

<!-- Optional theme -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap-theme.min.css"
      integrity="sha384-fLW2N01lMqjakBkx3l/M9EahuwpSfeNvV63J5ezn3uZzapT0u7EYsXMjQV+0En5r" crossorigin="anonymous">

<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"
        integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS"
        crossorigin="anonymous"></script>


<h1>Welcome to The Soft Shop</h1>

<button class="glyphicon glyphicon-plus">Add new</button>

<?php
/**
 * Created by PhpStorm.
 * User: coag
 * Date: 19-04-2016
 * Time: 10:54
 */

require_once("login.php");

$db = new PDO(
    "mysql:host=$host;dbname=$db_name;charset=utf8mb4",
    "$user",
    "$pass");

$sql_string = "SELECT * FROM prods";

$stmt = $db->query($sql_string);
?>
<table class="table table-striped">
    <tr>
        <th>id</th>
        <th>name</th>
        <th>price</th>
        <th>img</th>
        <th></th>
    </tr>
    <?php
    foreach ($stmt as $row) {
        echo "<tr>";
        echo '<td>' . $row['id'] . '</td>';
        echo '<td>' . $row['name'] . '</td>';
        echo '<td>' . $row['price'] . '</td>';
        echo '<td><img style="height: 100px" src="' . $row['img'] . '" alt="' . $row['name'] . '"></td>';
        echo '<td>
    <form method="post" action="delete_prod.php">
        <input type="hidden" name="id" value="' . $row['id'] . '">
        <button class="glyphicon glyphicon-trash" type="submit">Delete</button>
    </form>
    <button class="glyphicon glyphicon-refresh">Update</button></td>';
        echo "</tr>";

    }
    ?>
</table>
