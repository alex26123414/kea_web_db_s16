<h1>Blackjack Game</h1>
<?php
/**
 * Created by PhpStorm.
 * User: coag
 * Date: 15-03-2016
 * Time: 09:09
 */
global $cards, $total, $game;// = array();
$cards = array();
$types = array("club", "diamond", "heart", "spade");

for ($i = 1; $i <= 13; $i++) {
    for ($j = 0; $j < 4; $j++) {
        $card = "$i-$types[$j] ";
        array_push($cards, $card);
    }
}

$total = 0;
if (isset($_GET["game"])) {
    $game = $_GET["game"];
    $total = $_GET["total"];
    gen_cards(1);
} else {
    $game = "";
    $total = 0;
    gen_cards(2);
}


//var_dump($cards);

echo "<br>";
echo "<br>";

function gen_cards($NO_CARDS)
{
    global $cards, $total, $game;
    for ($i = 0; $i < $NO_CARDS; $i++) {
        do {
            $r = rand(0, count($cards) - 1);
            $card = $cards[$r];
        } while ($card == null);

        $game .= $card . "§";
        $temp = explode("-", $card);
        $total += $temp[0];
        $cards[$r] = null;
    }
}

$game_cards = explode("§", $game);
echo "<div style='display: inline-flex;'>";
for ($i = 0; $i < count($game_cards)-1; $i++) {
    $card = $game_cards[$i];
    $temp = explode("-", $card);
    $color = substr($temp[1], 0, 1);
    echo
        "<div style='font-size: 70px;
                     text-align: center;
                     background-size:cover;
                     background-image: url(\"img/" . $color . ".jpg\");
                     width: 100px;
                     height: 155px;'>
          $temp[0]
          </div>";
}
echo "</div>";
echo "<br>";
echo "<br>";

//var_dump($cards);


?>

<h1>Total: <?php echo $total; ?>
    <?php if ($total > 21) {
        echo " You lost</h1>";
    } ?>

    <form method="get">
        <input type="hidden" value="<?php echo $game; ?>" name="game">
        <input type="hidden" value="<?php echo $total; ?>" name="total">
        <button type="submit">Give me more!!</button>
    </form>
    <div style="width: 30px;"></div>
