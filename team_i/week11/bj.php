<h1>Welcome to the BJ Game</h1>
<?php
/**
 * Created by PhpStorm.
 * User: coag
 * Date: 14-03-2016
 * Time: 11:15
 */

global $cards, $total, $game;
$cards = array();
$types = array("club", "diamond", "heart", "spade");

for ($i = 1; $i < 14; $i++) {
    for ($t = 0; $t < count($types); $t++) {
        $type = $types[$t];
        $card = "$i-$type";
        array_push($cards, $card);
    }
}

if (isset($_POST["game"])) {
    $game = $_POST["game"];
    gen_cards(1);
    $total = get_total($game);
} else {
    $game = null;
    gen_cards(2);
    $total = get_total($game);
}

function gen_cards($no_of_cards){
    global $cards, $game;
    for ($i = 0; $i < $no_of_cards; $i++) {
        do {
            $r = rand(0, count($cards) - 1);
            $card = $cards[$r];
        } while ($card == null);
        $cards[$r] = null;
        $game .= $card . " ";
    }
}

function get_total($str_cards){
    $total = 0;
    $temp = explode(" ", $str_cards);
    for ($i = 0; $i < count($temp); $i++) {
        $total += $temp[$i];
    }
    return $total;
}

echo "<h1>Cards: $game </h1>";
echo "<h1>Total: $total </h1>";

?>

<form method="post">
    <input type="hidden" name="game" value="<?php echo $game; ?>">
    <button type="submit">Get extra</button>
</form>








