<?php
/**
 * Created by PhpStorm.
 * User: coag
 * Date: 04-04-2016
 * Time: 12:49
 */

//showData();
//
if(isset($_POST['cpr'])){
    $db = new PDO(
        'mysql:host=localhost;dbname=myfirstdb;charset=utf8mb4',
        'root',
        '');
    $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    $cpr = $_POST['cpr'];
    $name = (isset($_POST['name'])) ? $_POST['name'] : '';
    $gender = (isset($_POST['gender'])) ? $_POST['gender'] : '';
    $email = (isset($_POST['email'])) ? $_POST['email'] : '';
    $pass = (isset($_POST['pass'])) ? $_POST['pass'] : '';
    $year = (isset($_POST['year'])) ? $_POST['year'] : '';

    $stmt = $db->prepare("
        INSERT INTO `persons` (`cpr`, `name`, `gender`, `email`, `pass`, `year`)
        VALUES (:cpr, :name, :gender, :email, :pass, :year)
        ");

    $stmt->bindParam(':cpr', $cpr);
    $stmt->bindParam(':name', $name);
    $stmt->bindParam(':gender', $gender);
    $stmt->bindParam(':email', $email);
    $stmt->bindParam(':pass', $pass);
    $stmt->bindParam(':year', $year);


    $stmt->execute();
    showData();
}

function showData(){
    $db = new PDO(
        'mysql:host=localhost;dbname=myfirstdb;charset=utf8mb4',
        'root',
        '');
    $stmt = $db->query("SELECT * FROM persons");

    foreach ($stmt as $row) {
        echo 'CPR: ' . $row['cpr'] . '<br>';
        echo 'Name: ' . $row['name'] . '<br>';
        echo 'Gender: ' . $row['gender'] . '<br>';
        echo 'Email: ' . $row['email'] . '<br>';
        echo 'Pass: ' . $row['pass'] . '<br>';
        echo 'Year: ' . $row['year'] . '<br><br>';
    }
}

?>

<form method="post">
    <input type="text" name="cpr" required placeholder="CPR"><br>
    <input type="text" name="name" placeholder="Name"><br>
    <input type="text" name="gender" placeholder="Gender"><br>
    <input type="text" name="email" placeholder="Email"><br>
    <input type="text" name="pass" placeholder="Pass"><br>
    <input type="text" name="year" placeholder="Year"><br>
    <button type="submit">Insert</button>
    <button type="reset">Cancel</button>
</form>
