<?php
/**
 * Created by PhpStorm.
 * User: coag
 * Date: 07-03-2016
 * Time: 10:16
 * This are some examples that use random generations of nombers
 */
define("NO_OF_SPOTS", 3);
               //0123456789
$prices = array('img/car.jpg', 'img/coin.jpg', 'img/flag.jpg');


if (isset($_POST['rest_hist'])) {
    $history = '';
} else {
    if(isset($_POST['hist'])){
        $history = $_POST['hist'] . '<br>';
    }
    else {
        $history ='';
    }
}

    for ($i = 1; $i <= NO_OF_SPOTS; $i++) {
        $p = $prices[rand(0, 2)];
        $history .= substr($p, 4, -4) . ' ';
        ?>
        <img style="height: 75px" src="<?php echo $p; ?>"/>
        <?php
    }
?>

<form method="post">
    <button type="submit">GO!</button>
    <input type="checkbox" name="rest_hist"> Clear history
    <input type="hidden" name="hist" value="<?php echo $history; ?>">
</form>
<h3><?php echo $history; ?></h3>