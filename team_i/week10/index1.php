<?php
/**
 * Created by PhpStorm.
 * User: coag
 * Date: 07-03-2016
 * Time: 10:16
 * This are some examples that use random generations of nombers
 */
define("START_CREDIT", 10);
define("NO_OF_SPOTS", 3);
session_start();

$prices = array('img/car.jpg', 'img/coin.jpg', 'img/flag.jpg');
$wins   = array(100,            20,             5);

if (!isset($_SESSION['credit'])) {
    $credit = START_CREDIT;
} else{
    $credit = $_SESSION['credit'];
}




if (isset($_POST['rest_hist'])) {
    $history = '';
} else {
    if(isset($_POST['hist'])){
        $history = $_POST['hist'] . '<br>';
    }
    else {
        $history ='';
    }
}



if ($credit > 0) {
    $iswin = true;
    for ($i = 1; $i <= NO_OF_SPOTS; $i++) {
        $new = rand(0, 2);
        if(!isset($old)){
            $old = $new;
        }
        if($old != $new){
            $iswin = false;
        }
        $old = $new;
        $p = $prices[$new];
        $history .= substr($p, 4, -4) . ' ';
        ?>
        <img style="height: 75px" src="<?php echo $p; ?>"/>
        <?php
    }

    if($iswin){
        $credit += $wins[$new];
    }
} else{
    die("No more credit <img src='img/buy.gif'/>");
}
$credit--;
$_SESSION['credit'] = $credit;
?>

<form method="post">
    <h1>Credit: <?php echo $credit; ?></h1>
    <button type="submit">GO!</button>
    <input type="checkbox" name="rest_hist"> Clear history
    <input type="hidden" name="hist" value="<?php echo $history; ?>">
</form>
<h1><?php echo $history; ?></h1>